using Microsoft.AspNetCore.Mvc.Rendering;

using System.Collections.Generic;

namespace BrainDump.Models
{
    public class IssueLabelViewModel
    {
        public List<Issue> Issues { get; set; }
        public SelectList Labels { get; set; }
        public string Label { get; set; }
        public string SearchString { get; set; }
    }
}
