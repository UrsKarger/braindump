using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using BrainDump.Data;

using System;
using System.Linq;

namespace BrainDump.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ApplicationDbContext>>()))
            {
                // Look for any movies.
                if (context.Issue.Any())
                {
                    return;   // DB has been seeded
                }
                context.Issue.AddRange(
                    new Issue
                    {
                        Title = "make baby",
                        IssueId = "Kid-0001",
                        CreationDate = DateTime.Parse("2021-05-03T22:00:00"),
                        Description = "we need a baby",
                        Epic = "baby",
                        Module = "-",
                        Assignee = "Mother+Father",
                        State = "InProgress",
                        Labels = "before",
                        Link = "",
                        Priority = "Low"
                    },
                    new Issue
                    {
                        Title = "find out the sex of the baby",
                        IssueId = "Kid-0002",
                        CreationDate = DateTime.Parse("2021-05-03T22:00:00"),
                        Description = "we need a baby",
                        Epic = "baby",
                        Module = "-",
                        Assignee = "Mother+Father",
                        State = "InProgress",
                        Labels = "pregnancy",
                        Link = "",
                        Priority = "Low"
                    },
                    new Issue
                    {
                        Title = "find a name for the baby",
                        IssueId = "Kid-0003",
                        CreationDate = DateTime.Parse("2021-05-03T22:00:00"),
                        Description = "we need a baby",
                        Epic = "baby",
                        Module = "-",
                        Assignee = "Mother+Father",
                        State = "InProgress",
                        Labels = "pregnancy",
                        Link = "",
                        Priority = "Low"
                    },
                    new Issue
                    {
                        Title = "find a baby bed",
                        IssueId = "Kid-0004",
                        CreationDate = DateTime.Parse("2021-05-03T22:00:00"),
                        Description = "we need a baby",
                        Epic = "baby",
                        Module = "-",
                        Assignee = "Father",
                        State = "InProgress",
                        Labels = "born",
                        Link = "",
                        Priority = "Low"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
