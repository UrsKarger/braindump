using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;


namespace BrainDump.Models
{
    public class Issue
    {
        public int Id { get; set; }

        [StringLength(120, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }

        [RegularExpression(@"^[A-Za-z]\w*-\d+$")]
        [Required]
        public string IssueId { get; set; }

        [Display(Name = "Creation Date")]
        [DataType(DataType.DateTime)]
        public DateTime CreationDate { get; set; }
        public string Description { get; set; }

        //[Comment("link to an epic")]
        public string Epic { get; set; }

        public string Module { get; set; }

        public string Assignee { get; set; }

        public string State { get; set; }

        public string Labels { get; set; }

        public string Link { get; set; }

        public string Priority { get; set; }
    }
}
