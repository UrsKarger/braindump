using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BrainDump.Configurations
{

    public class DefaultConfigurationProvider : ConfigurationProvider
    {
        public DefaultConfigurationProvider()
        {
        }

        public override void Load()
        {
            string AppName = "BrainDump";
            string DataDirectory = Path.Join(Environment.GetEnvironmentVariable("ProgramData"), AppName, "database", "default.db");

            var configurationValues = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                ["Database:DbType"] = "SQLite",
                ["Database:SQLite:file"] = DataDirectory
            };

            foreach (var x in configurationValues)
            {
                Data[x.Key] = x.Value;
            }

        }

    }


public class DefaultConfigurationSource : IConfigurationSource
{

    public DefaultConfigurationSource()
    {

    }

    public IConfigurationProvider Build(IConfigurationBuilder builder)
    {
        return new DefaultConfigurationProvider();
    }
}
}

public static class EntityFrameworkExtensions
{
    public static IConfigurationBuilder AddDefaultConfiguration(
        this IConfigurationBuilder builder)
    {
        return builder.Add(new BrainDump.Configurations.DefaultConfigurationSource());
    }
}

