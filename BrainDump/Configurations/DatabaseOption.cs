using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrainDump.Configurations
{
    /// <summary>
    /// possible database options
    /// </summary>
    public enum DatabaseOption
    {
        SQLite,  /// <summary> SQLite </summary>
        Postgres /// <summary> Postgres </summary>
    };
}
