using BrainDump.Data;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Serilog;

namespace BrainDump.Configurations
{
    /// <summary>
    /// Configuration for the DI part
    /// </summary>
    public class DependencyInjection
    {
        /// <summary>
        /// Configure the dependency Injection
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="configuration">global configuration</param>
        public static void ConfigureDependencyInjection(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IAppConfiguration, AppConfiguration>();
            // services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
        }
    }
} // namespace BrainDump.Configurations
