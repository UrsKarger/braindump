using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrainDump.Configurations
{
    /// <summary>
    /// provide a typed interface for configuration access
    /// </summary>
    public interface IAppConfiguration
    {
        /// <summary>
        /// the database type the app uses
        /// </summary>
        /// <returns>the configured database option</returns>
       DatabaseOption ConfiguredDatabase();
    }
}
