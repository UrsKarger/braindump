using System;
using Microsoft.Extensions.Configuration;

namespace BrainDump.Configurations
{

    /// <summary>
    /// The Configuration of the application
    /// </summary>
    public class AppConfiguration : IAppConfiguration
    {
        private readonly IConfiguration _configuration;
        public AppConfiguration(IConfiguration config)
        {
            _configuration = config;
        }
        public DatabaseOption ConfiguredDatabase()
        {
            string val = _configuration["Database:DbType"];
            return (DatabaseOption) Enum.Parse(typeof(DatabaseOption), _configuration["Database:DbType"], false);
        }
    }
}
