using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BrainDump.Data;
using BrainDump.Models;
using BrainDump.Configurations;

using Serilog;
using Serilog.AspNetCore;
using Serilog.Events;

namespace BrainDump
{
    public class Program
    {

        /// <summary>
        /// App Main
        /// </summary>
        /// <param name="args">command line arguments</param>
        public static void Main(string[] args)
        {
            string app_dir = Environment.GetEnvironmentVariable("APPDATA") ?? Environment.GetEnvironmentVariable("PROGRAMDATA");

            string log_dir = Path.Join(app_dir, "BrainDump", "log");
            Directory.CreateDirectory(log_dir);


            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .WriteTo.File(Path.Join(log_dir, "logging.log"), rollingInterval: RollingInterval.Day)
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Warning)
                .CreateBootstrapLogger();
                // .CreateLogger();

            try
            {

                Log.Information("App is starting");
                var host = CreateHostBuilder(args).Build();

                using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;

                    try
                    {
                        SeedData.Initialize(services);
                    }
                    catch (Exception ex)
                    {
                        var logger = services.GetRequiredService<ILogger>();
                        logger.Error(ex, "An error occurred seeding the DB.");
                    }
                }

                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// create the host builder
        /// </summary>
        /// <param name="args">command line arguments</param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var switchMappings_p1 = new Dictionary<string, string>()
            { 
                { "--cfg", "json_cfg" },
                { "--sqlite", "sql_dir" }
            };

            string app_dir = Environment.GetEnvironmentVariable("APPDATA") ?? Environment.GetEnvironmentVariable("PROGRAMDATA");
            string log_dir = Path.Join(app_dir, "BrainDump", "log");

            var pre_config = new ConfigurationBuilder()
                 .AddCommandLine(args, switchMappings_p1)
                .Build();
            string extra_json_cfg = pre_config["json_cfg"];
            string sql_lite = pre_config["sql_dir"];

            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.Sources.Clear();

                    var env = hostingContext.HostingEnvironment;

                    config.AddCommandLine(args, switchMappings_p1);
                    config.AddDefaultConfiguration();
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json",
                                         optional: true, reloadOnChange: true);
                    config.AddJsonFile(extra_json_cfg, optional: true, reloadOnChange: false);
                    config.AddEnvironmentVariables();
                })
                // .UseSerilog()
                .UseSerilog((context, services, configuration) => configuration
                    .WriteTo.Console()
                    .WriteTo.File(Path.Join(log_dir, "logging.log"), rollingInterval: RollingInterval.Day)
                    .ReadFrom.Configuration(context.Configuration)
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext(), writeToProviders: true)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}
