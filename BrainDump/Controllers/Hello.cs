using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace BrainDump.Controllers
{
    public class Hello : Controller
    {
        // 
        // GET: /Hello/
        public IActionResult Index()
        {
            return View();
        }

        // GET: /Hello/Welcome/ 
        // Requires using System.Text.Encodings.Web;
        public IActionResult Welcome(string name, int numTimes = 1)
        {
            ViewData["Message"] = "Hello " + name;
            ViewData["NumTimes"] = numTimes;

            return View();
        }
    }
}
