using System;
using System.IO;

namespace BrainDump.FileIO
{ 

/// <summary>
/// Storage of Data in a File based database
/// </summary>
public interface IFileStorage
{

    /// <summary>
    ///
    /// </summary>
    string Read();

    /// <summary>
    /// Write the data to the opened file
    /// </summary>
    void Write(string json);

    /// <summary>
    /// open the file database with the given file name
    /// </summary>
    bool Open(string filename);
}

} // namespace BrainDump.FileIO
