using System;
using System.IO;
using System.Text;

namespace BrainDump.FileIO
{
    /// <summary>
    /// File Storage implementation
    /// </summary>
    public class FileStorage : IFileStorage
    {
        private FileStream _db = null;

        public FileStorage(string dbpath)
        {
            try
            {
                if (!File.Exists(dbpath))
                {
                    _db = new FileStream(dbpath, FileMode.CreateNew);
                }
                else
                {
                    _db = new FileStream(dbpath, FileMode.Open);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentException($"db could not be opened {e.Message}");
            }
        }

        public string Read()
        {
            string retVal = "";
            byte[] block = new byte[1024];
            UTF8Encoding temp = new UTF8Encoding(true);
            while (_db.Read(block, 0, block.Length) > 0)
            {
                retVal = temp.GetString(block);
            }

            return retVal;
        }

        /// <summary>
        /// Write the data to the opened file
        /// </summary>
        public void Write(string json)
        {
            AddText(_db, json);
        }

        /// <summary>
        /// open the file database with the given file name. If it does not exist
        /// a new one will be created.
        /// </summary>
        /// <param name="dbpath">database file</param>
        /// <returns>true if successful, false otherwise</returns>
        public bool Open(string dbpath)
        {
            if (_db != null)
            {
                _db.Close();
                _db.Dispose();
                _db = null;
            }

            if (!File.Exists(dbpath))
            {
                _db = new FileStream(dbpath, FileMode.CreateNew);
            }
            else
            {
                _db = new FileStream(dbpath, FileMode.Open);
            }

            return (_db == null);
        }

        private static void WriteHeader(FileStream fs)
        {
            // project info
            // - name
            // - description
            // - 
            AddText(fs, Convert.ToChar(1024).ToString());
        }

        private static void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }

    }
} // namespace BrainDump.FileIO
