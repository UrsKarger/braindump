using System;
using System.IO;
using System.Text;

using Microsoft.Extensions.Logging;

namespace BrainDump.FileIO
{
    /// <summary>
    /// A file database is basically a directory with data files in it.
    /// The root directory can have subdirectories.
    /// </summary>
    public class FileDatabase
    {
        private const string DB_INFO = "FileDb.info";
        private string _path;
        private readonly ILogger _logger;

        /// <summary>
        /// The constructor opens a database at the given location
        /// If there is no database at path one will be created
        /// </summary>
        /// <param name="path">database root directory</param>
        public FileDatabase(ILogger<FileDatabase> logger, string path)
        {
            _logger = logger;
            _path = path;

            if (Directory.Exists(path))
            {
                var dbInfoFile = System.IO.Path.Join(path, DB_INFO);
                if (File.Exists(dbInfoFile))
                {

                }
                else
                {
                    File.Create(dbInfoFile);
                }
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(path);
                    File.Create(System.IO.Path.Join(path, DB_INFO));
                }
                catch (UnauthorizedAccessException e)
                {
                    _logger.LogError($"access to {path} is not authorized: {e.Message}");
                    throw new ArgumentException($"Invalid parameter path: {path}");
                }
            }

        }
    }
}
